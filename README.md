#WIMAFLEX

A small CSS framework for flex-based website intended for private use in WIMA

Well, it's supposed to be modular. More things will be added in the future.

##GRIDS
+ **.Grid**  
   The mother of all blocks. Every block that utilize grid needs to be encapsulated with this class.
+ **.Grid--gutter**  
   Enables gutter for **.Grid**. Else, they will be right at next each other.
+ **.Grid-cell**  
   The children of .Grid. Defaults to flex: 1 a.k.a. auto fit size.
+ **.'size'-Grid--'cellAmount' or without prefix**  
   Intended to be used on the same level as .Grid; behaves the same as col-[sm/xs etc] in bootstrap.
   + size options  
      + small
      + medium
      + large
      + xlarge
      + xxlarge
   + cellAmount options
      + fit
      + oneCell
      + twoCells
      + threeCells
      + fourCells  
   For example: .small-Grid--fit, .large-Grid--threeCells, etc.
+ **.Grid--'alignment'**  
   To align all cells inside .Grid respective to the suffix.
   + alignment options
      + alignTop
      + alignCenter
      + alignBottom  
   For example: .Grid--alignTop, .Grid--alignCenter, etc.
+ **.Grid-cell--'cellAlignment'**  
   To align cells inside .Grid, individually.
   + cellAlignment options
      + top
      + center
      + bottom  
   For example: .Grid-cell--top, .Grid-cell--center, etc.
+ **.u-halfWidth--'utilitySize' or without suffix**  
   To set the width of individual cells. Do not add if you want auto.
   + utilitySize options
      + small
      + medium
      + large
      + xlarge
      + xxlarge  
   For example: .u-halfWidth--small, .u-halfWidth--large, etc.

*This work is MIT licensed. Heavily influenced by [Solved-by-flexbox by Philip Walton](http://philipwalton.github.io/solved-by-flexbox)*