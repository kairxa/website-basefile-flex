var gulp = require('gulp');

var jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    minify = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    del = require('del');

gulp.task('lint', function() {
    return gulp.src(['assets/javascript/**/*.js', '!assets/javascript/**/all*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('scss', function() {
    return gulp.src('assets/stylesheet/**/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('assets/tmp'))
        .pipe(concat('all.css'))
        .pipe(gulp.dest('assets/stylesheet'))
        .pipe(rename('all.min.css'))
        .pipe(minify())
        .pipe(gulp.dest('assets/stylesheet'))
        .on('end', function() {
            del('assets/tmp/', function(err) {
                if(!err)
                    console.log('Cleanup Successful');
                else
                    console.log(err);
            });
        });
});

gulp.task('javascript', function() {
    return gulp.src(['assets/javascript/**/*.js', '!assets/javascript/**/all*.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('assets/javascript'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/javascript'));
});

gulp.task('watch', function() {
    gulp.watch(['assets/javascript/**/*.js', '!assets/javascript/**/all*.js'], ['lint', 'javascript']);
    gulp.watch('assets/stylesheet/**/*.scss', ['scss']);
});

gulp.task('default', ['lint', 'scss', 'javascript', 'watch']);